/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import fs from 'fs-extra'
import JSZip from 'jszip'
import tempy from 'tempy'
import { convert } from '../convert'
import { convertExtyles } from '../extyles'

const XLINK_NAMESPACE = 'http://www.w3.org/1999/xlink'

const inputPath = __dirname + '/../../data/example.manuproj'
const extylesInputPath = __dirname + '/../../data/extyles.zip'

describe('Convert', () => {
  test('to JATS', async () => {
    const outputPath = tempy.file()

    await convert(inputPath, 'jats', outputPath, { jatsVersion: '1.2' })

    const output = await fs.readFile(outputPath)
    const zip = await new JSZip().loadAsync(output)

    const files = Object.keys(zip.files)
    expect(files).toEqual([
      'graphic/',
      'graphic/MPFigure_5530A696-5001-49B9-B953-0242CC50D400.png',
      'manuscript.xml',
    ])

    const xml = await zip.file('manuscript.xml').async('text')
    const parser = new DOMParser()
    const doc = parser.parseFromString(xml, 'application/xml')

    const nodes = Array.from(doc.querySelectorAll('graphic'))
    expect(nodes).toHaveLength(1)

    const urls = nodes.map(node => node.getAttributeNS(XLINK_NAMESPACE, 'href'))
    expect(urls).toEqual([
      'graphic/MPFigure_5530A696-5001-49B9-B953-0242CC50D400.png',
    ])
  })

  test('to JATS for Literatum', async done => {
    const outputPath = tempy.file()

    await convert(inputPath, 'literatum-jats', outputPath, {
      jatsVersion: '1.2d1',
      doi: '10.0000/123',
    })

    const output = await fs.readFile(outputPath)
    const zip = await new JSZip().loadAsync(output)

    const files = Object.keys(zip.files)
    expect(files).toEqual([
      'graphic/',
      'graphic/MPFigure_5530A696-5001-49B9-B953-0242CC50D400.png',
      '123.xml',
    ])

    const xml = await zip.file('123.xml').async('text')
    const parser = new DOMParser()
    const doc = parser.parseFromString(xml, 'application/xml')

    const nodes = Array.from(doc.querySelectorAll('graphic'))
    expect(nodes).toHaveLength(1)

    const urls = nodes.map(node => node.getAttributeNS(XLINK_NAMESPACE, 'href'))
    expect(urls).toEqual(['MPFigure_5530A696-5001-49B9-B953-0242CC50D400.png'])

    done()
  })

  test('to HTML', async () => {
    const outputPath = tempy.file()

    await convert(inputPath, 'html', outputPath, {})

    const output = await fs.readFile(outputPath)
    const zip = await new JSZip().loadAsync(output)

    const files = Object.keys(zip.files)
    expect(files).toEqual([
      'Data/',
      'Data/MPFigure_5530A696-5001-49B9-B953-0242CC50D400.png',
      'index.html',
    ])

    const html = await zip.file('index.html').async('text')
    const parser = new DOMParser()
    const doc = parser.parseFromString(html, 'text/html')

    const nodes = Array.from(doc.querySelectorAll('img'))
    expect(nodes).toHaveLength(1)

    const urls = nodes.map(node => node.getAttribute('src'))
    expect(urls).toEqual([
      'Data/MPFigure_5530A696-5001-49B9-B953-0242CC50D400.png',
    ])
  })

  test.skip('to PDF', async () => {
    const outputPath = tempy.file()
    // console.log(outputPath)

    await convert(inputPath, 'pdf', outputPath, {})

    const stat = await fs.stat(outputPath)

    expect(stat.size).toBeGreaterThan(500000)
  })

  test('to Digital Objects for Literatum', async () => {
    const outputPath = tempy.file()

    await convert(inputPath, 'literatum-do', outputPath, {
      doi: '10.0000/123',
      doType: 'test',
    })

    const output = await fs.readFile(outputPath)
    const zip = await new JSZip().loadAsync(output)

    const files = Object.keys(zip.files)
    expect(files).toEqual([
      'manifest.xml',
      '123/',
      '123/Data/',
      '123/Data/MPFigure_5530A696-5001-49B9-B953-0242CC50D400.png',
      '123/meta/',
      '123/meta/123.xml',
    ])

    const meta = await zip.file('123/meta/123.xml').async('text')
    const parser = new DOMParser()
    const doc = parser.parseFromString(meta, 'application/xml')

    const namespaces: { [key: string]: string } = {
      atpn: 'http://www.atypon.com/digital-objects',
      mets: 'http://www.loc.gov/METS/',
    }

    const bodyNode = doc.evaluate(
      '//atpn:body',
      doc,
      prefix => (prefix ? namespaces[prefix] : null),
      XPathResult.FIRST_ORDERED_NODE_TYPE
    ).singleNodeValue

    expect(bodyNode).not.toBeNull()
    expect(bodyNode!.textContent).toMatchSnapshot()

    const result = doc.evaluate(
      '//mets:fileSec/mets:fileGrp/mets:file',
      doc,
      prefix => (prefix ? namespaces[prefix] : null),
      XPathResult.ORDERED_NODE_SNAPSHOT_TYPE
    )

    const fileNodes = Array.from(
      { length: result.snapshotLength },
      (_, index) => result.snapshotItem(index)
    )

    expect(fileNodes).toHaveLength(1)

    const fileElement = fileNodes[0] as Element
    expect(fileElement.getAttribute('ID')).toBe(
      'MPFigure_5530A696-5001-49B9-B953-0242CC50D400'
    )
  })

  test.skip('to JATS bundle', async () => {
    const outputPath = tempy.file()

    await convert(inputPath, 'jats-bundle', outputPath, {
      doi: '10.0000/123',
      groupDoi: '10.0000/test.just-accepted',
    })

    const output = await fs.readFile(outputPath)
    const zip = await new JSZip().loadAsync(output)

    const files = Object.keys(zip.files).sort()

    expect(files).toEqual([
      'manifest.xml',
      'test.just-accepted/',
      'test.just-accepted/123/',
      'test.just-accepted/123/123.pdf',
      'test.just-accepted/123/123.xml',
      'test.just-accepted/123/graphic/',
      'test.just-accepted/123/graphic/MPFigure_5530A696-5001-49B9-B953-0242CC50D400.png',
    ])

    const meta = await zip.file('test.just-accepted/123/123.xml').async('text')
    const parser = new DOMParser()
    const doc = parser.parseFromString(meta, 'application/xml')
    expect(doc.documentElement.nodeName).toBe('article')
  })

  test.skip('to WileyML bundle', async () => {
    const outputPath = tempy.file()

    await convert(inputPath, 'wileyml-bundle', outputPath, {
      doi: '10.0000/123',
      groupDoi: '10.0000/test.just-accepted',
    })

    const output = await fs.readFile(outputPath)
    const zip = await new JSZip().loadAsync(output)

    const files = Object.keys(zip.files)
    expect(files).toEqual([
      'manifest.xml',
      'test.just-accepted/',
      'test.just-accepted/123/',
      'test.just-accepted/123/123.pdf',
      'test.just-accepted/123/123.xml',
      'test.just-accepted/123/graphic/',
      'test.just-accepted/123/graphic/MPFigure_5530A696-5001-49B9-B953-0242CC50D400.png',
    ])

    const meta = await zip.file('test.just-accepted/123/123.xml').async('text')
    const parser = new DOMParser()
    const doc = parser.parseFromString(meta, 'application/xml')
    expect(doc.documentElement.nodeName).toBe('component')

    // TODO: validate with DTD
  })

  test.skip('to WileyML bundle from eXtyles', async () => {
    const outputPath = tempy.file({
      extension: 'zip',
    })

    console.log(outputPath)

    await convertExtyles(extylesInputPath, 'wileyml-bundle', outputPath, {
      doi: '10.0000/123',
      groupDoi: '10.0000/test.just-accepted',
    })

    const output = await fs.readFile(outputPath)
    const zip = await new JSZip().loadAsync(output)

    const files = Object.keys(zip.files).sort()

    expect(files).toEqual([
      'manifest.xml',
      'test.just-accepted/',
      'test.just-accepted/123/',
      'test.just-accepted/123/123.pdf',
      'test.just-accepted/123/123.xml', // TODO: validate
    ])

    const meta = await zip.file('test.just-accepted/123/123.xml').async('text')
    const parser = new DOMParser()
    const doc = parser.parseFromString(meta, 'application/xml')
    expect(doc.documentElement.nodeName).toBe('component')
  })

  test.skip('to Gateway bundle from eXtyles', async () => {
    const outputPath = tempy.file({
      extension: 'zip',
    })

    console.log(outputPath)

    await convertExtyles(extylesInputPath, 'gateway-bundle', outputPath, {
      doi: '10.0000/123',
      groupDoi: '10.0000/test.just-accepted',
      issn: '1234-5678',
    })

    const output = await fs.readFile(outputPath)
    const zip = await new JSZip().loadAsync(output)

    const files = Object.keys(zip.files).sort()

    expect(files).toEqual([
      '12345678/',
      '12345678/9999/',
      '12345678/9999/9999/',
      '12345678/9999/9999/999A/',
      '12345678/9999/9999/999A/123/',
      '12345678/9999/9999/999A/123/123.pdf',
      '12345678/9999/9999/999A/123/123.xml', // TODO: validate
    ])

    const meta = await zip
      .file('12345678/9999/9999/999A/123/123.xml')
      .async('text')
    const parser = new DOMParser()
    const doc = parser.parseFromString(meta, 'application/xml')
    expect(doc.documentElement.nodeName).toBe('component')
  })
})
