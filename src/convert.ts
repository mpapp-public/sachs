/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  findManuscript,
  HTMLTransformer,
  JATSExporter,
  parseProjectBundle,
  ProjectBundle,
  Version,
} from '@manuscripts/manuscript-transform'
import { execFileSync } from 'child_process'
import fs from 'fs'
import JSZip from 'jszip'
import tempy from 'tempy'
import { copyHTMLDataFiles, copyXMLDataFiles } from './data'
import { setGlobals } from './globals'
import { addJournalMetadata } from './journal'
import { isValidDOI } from './lib'
import { buildManifest } from './manifest'
import { buildContainer } from './mets'
import { convertToPDF } from './pdf'

export type OutputFormat =
  | 'html'
  | 'jats'
  | 'jats-bundle'
  | 'literatum-jats'
  | 'literatum-do'
  | 'manuproj'
  | 'pdf'
  | 'wileyml-bundle'
  | 'gateway-bundle'

export const supportedInputFormats = () => ['extyles', 'manuproj']

export const supportedOutputFormats = () => [
  'html',
  'jats',
  'jats-bundle',
  'literatum-jats',
  'literatum-do',
  'manuproj',
  'pdf',
  'wileyml-bundle',
  'gateway-bundle',
]

export interface Options {
  doi?: string
  doType?: string
  frontMatterOnly?: boolean
  groupDoi?: string
  issn?: string
  jatsVersion?: Version
}

// tslint:disable-next-line:cyclomatic-complexity
export const convert = async (
  inputPath: string,
  format: OutputFormat,
  outputPath: string,
  options: Options
): Promise<string | void> => {
  const { doi, doType, frontMatterOnly, groupDoi, jatsVersion } = options

  setGlobals()

  const data = fs.readFileSync(inputPath)

  const zip = await new JSZip().loadAsync(data)

  const json = await zip.file('index.manuscript-json').async('text')

  const projectBundle: ProjectBundle = JSON.parse(json)

  const { doc, modelMap } = parseProjectBundle(projectBundle)

  const output = new JSZip()

  const writeOutput = (): Promise<string | void> =>
    new Promise((resolve, reject) => {
      output
        .generateNodeStream({
          type: 'nodebuffer',
          streamFiles: true,
        })
        .pipe(fs.createWriteStream(outputPath))
        .on('error', error => {
          reject(error)
        })
        .on('finish', () => {
          resolve()
        })
    })

  switch (format) {
    case 'jats': {
      const transformer = new JATSExporter()

      let xml = transformer.serializeToJATS(
        doc.content,
        modelMap,
        jatsVersion,
        undefined,
        undefined,
        frontMatterOnly
      )
      xml = await copyXMLDataFiles(xml, zip, output)

      output.file('manuscript.xml', xml)

      return writeOutput()
    }

    case 'literatum-jats': {
      if (!doi) {
        throw new Error('doi parameter is required')
      }

      if (!isValidDOI(doi)) {
        throw new Error('Invalid DOI format')
      }

      const [, id] = doi.split('/', 2)

      const transformer = new JATSExporter()

      let xml = transformer.serializeToJATS(
        doc.content,
        modelMap,
        '1.2d1', // TODO: make this the default
        doi,
        id,
        frontMatterOnly
      )
      xml = await copyXMLDataFiles(xml, zip, output, true)

      output.file(`${id}.xml`, xml)

      return writeOutput()
    }

    case 'html': {
      const transformer = new HTMLTransformer()

      const html = transformer.serializeToHTML(doc.content, modelMap)

      await copyHTMLDataFiles(html, zip, output)

      output.file('index.html', html)

      return writeOutput()
    }

    case 'literatum-do': {
      if (!doi) {
        throw new Error('doi parameter is required')
      }

      if (!doType) {
        throw new Error('doType parameter is required')
      }

      const [, id] = doi.split('/', 2)

      const manuscript = findManuscript(modelMap)

      const transformer = new HTMLTransformer()
      const html = transformer.serializeToHTML(doc.content, modelMap)

      output.file(
        'manifest.xml',
        buildManifest({
          groupDoi: '10.5555/default-do-group',
          submissionType: 'full',
        })
      )

      const files = await copyHTMLDataFiles(html, zip, output, `${id}/Data/`)

      const container = buildContainer(
        html,
        files,
        manuscript,
        modelMap,
        doType
      )
      output.file(`${id}/meta/${id}.xml`, container)

      return writeOutput()
    }

    case 'jats-bundle':
    case 'wileyml-bundle': {
      if (!doi || !groupDoi) {
        throw new Error('doi and group-doi parameters are required')
      }

      if (!isValidDOI(doi)) {
        throw new Error('Invalid DOI format')
      }

      output.file(
        'manifest.xml',
        buildManifest({
          groupDoi,
          processingInstructions: {
            priorityLevel: 'high',
            // makeLiveCondition: 'no-errors',
          },
          submissionType: 'partial',
        })
      )

      const [, groupId] = groupDoi.split('/', 2)
      const groupFolder = output.folder(groupId)

      const [, id] = doi.split('/', 2)
      const articleFolder = groupFolder.folder(id)

      // PDF

      const htmlTransformer = new HTMLTransformer()

      const html = htmlTransformer.serializeToHTML(doc.content, modelMap)

      const tempZip = new JSZip()
      tempZip.file('index.html', html)
      await copyHTMLDataFiles(html, zip, tempZip)

      const tempPDFFile = tempy.file()
      await convertToPDF(tempZip, 'article', tempPDFFile)

      const pdfPath = `${id}.pdf`
      articleFolder.file(pdfPath, fs.createReadStream(tempPDFFile))

      // XML

      const inputPath = tempy.file()
      const outputPath = tempy.file()

      const links = { self: { pdf: pdfPath } }

      const xmlTransformer = new JATSExporter()

      let xml = xmlTransformer.serializeToJATS(
        doc.content,
        modelMap,
        jatsVersion, // 1.1?
        doi,
        id,
        frontMatterOnly, // true?
        links
      )

      xml = await copyXMLDataFiles(xml, zip, articleFolder, true)

      if (format === 'wileyml-bundle') {
        // convert JATS to WileyML
        xml = addJournalMetadata(xml)

        fs.writeFileSync(inputPath, xml)

        execFileSync('jats-to-wileyml', [inputPath, outputPath])

        articleFolder.file(`${id}.xml`, fs.createReadStream(outputPath))
      } else {
        articleFolder.file(`${id}.xml`, xml)
      }

      return writeOutput()
    }

    case 'manuproj': {
      const data = Array.from(modelMap.values())
      const json = JSON.stringify({ version: '2.0', data })
      output.file('index.manuscript-json', json)

      return writeOutput()
    }

    case 'pdf': {
      const transformer = new HTMLTransformer()

      const html = transformer.serializeToHTML(doc.content, modelMap)

      await copyHTMLDataFiles(html, zip, output)

      output.file('index.html', html)

      return convertToPDF(output, 'article', outputPath)
    }

    default: {
      throw new Error('Unknown format')
    }
  }
}
