#!/usr/bin/env node

/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { supportedVersions, Version } from '@manuscripts/manuscript-transform'
import debug from 'debug'
import yargs from 'yargs'
import { version } from '../package.json'
import {
  convert,
  OutputFormat,
  supportedInputFormats,
  supportedOutputFormats,
} from './convert'
import { convertExtyles } from './extyles'

debug.enable('manuscripts-transform')

interface Args {
  doi?: string
  doType?: string
  format: OutputFormat
  frontMatterOnly?: boolean
  groupDoi?: string
  input: string
  inputFormat?: string
  issn?: string
  jatsVersion?: Version
  output: string
}

const {
  argv: {
    doi,
    doType,
    format,
    frontMatterOnly,
    groupDoi,
    input,
    inputFormat,
    issn,
    jatsVersion,
    output,
  },
} = yargs
  .command<Args>('export', 'Convert a file.', yargs =>
    yargs
      .option('input', {
        describe: 'Path to the input file',
        type: 'string',
      })
      .option('output', {
        describe: 'Path to the output file',
        type: 'string',
      })
      .option('input-format', {
        describe: 'Format to convert from',
        type: 'string',
        choices: supportedInputFormats(),
      })
      .option('format', {
        describe: 'Format to convert to',
        type: 'string',
        choices: supportedOutputFormats(),
      })
      .option('jats-version', {
        describe: 'JATS version',
        type: 'string',
        choices: supportedVersions(),
      })
      .option('doi', {
        describe: 'Manuscript DOI',
        type: 'string',
      })
      .option('issn', {
        describe: 'ISSN',
        type: 'string',
      })
      .option('group-doi', {
        describe: 'Group DOI',
        type: 'string',
      })
      .option('do-type', {
        describe: 'Digital Objects type',
        type: 'string',
      })
      .option('front-matter-only', {
        describe: 'Only include the front matter',
        type: 'boolean',
      })
      .demandOption('input', 'Provide the path to the input file')
      .demandOption('output', 'Provide the path to the output file')
      .demandOption('format', 'Provide the format for the output')
  )
  .scriptName('sachs')
  .version(version)
  .usage('Usage: $0 <command> --format [format] --input [file] --output [file]')
  .help()
  .demandCommand()

console.log(`Converting ${input} to ${output} as ${format}`)

const converter = inputFormat === 'extyles' ? convertExtyles : convert

converter(input, format, output, {
  doi,
  doType,
  frontMatterOnly,
  groupDoi,
  issn,
  jatsVersion,
})
  .then(() => {
    console.log('Converted')
  })
  .catch((error: Error) => {
    console.error(error)
    process.exitCode = 1
  })
