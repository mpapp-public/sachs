module.exports = {
  moduleFileExtensions: ['ts', 'tsx', 'js', 'json'],
  // setupFilesAfterEnv: ['./src/tests.ts'],
  testPathIgnorePatterns: ['/node_modules/'],
  testRegex: '__tests__.*\\.test\\.tsx?$',
  transform: {
    '^.+\\.ts$': 'babel-jest',
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/(?!(lodash-es|@manuscripts)/)'],
}
